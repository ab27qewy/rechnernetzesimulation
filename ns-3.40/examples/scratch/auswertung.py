import csv
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as st

def calculate_average(file_path, file_name, simulation):
    full_path = file_path + file_name
    count = 0
    total_values = 0

    try:
        with open(full_path, 'r', newline='') as csvfile:
            # Read the CSV file
            csv_reader = csv.reader(csvfile)

            # Skip the header
            next(csv_reader)

            # Extract data based on the specified case
            for row in csv_reader:
                if simulation == "goodput":
                    time, value = map(float, row)
                    if value != 0:
                        count += 1
                        total_values += value
                elif simulation == "transmission":
                    if float(row[1]) != 0:
                        total_values += float(row[1])
                        count += 1
            print(f'Datei "{full_path}" erfolgreich eingelesen.')
    except FileNotFoundError:
        print(f'Der Ordner "{full_path}" wurde nicht gefunden.')
        return 0  # Returning 0 immediately if file not found
    except Exception as e:
        print(f'Es gab ein Fehler beim Einlesen der Datei "{full_path}": {e}')
        return 0  # Returning 0 immediately if other error occurs

    # Check if count is zero and handle it
    if count == 0:
        print(f'Keine Nicht-Null Werte für {simulation} in {full_path} gefunden. Rückgabe 0.')
        return 0

    # Calculate and return the average based on the case
    if simulation == "goodput":
        return (total_values * 40) / count
    elif simulation == "transmission":
        return total_values / count

# Function to calculate Jain's Fairness Index
def calculate_fairness(szenario):

    devices = {"bulk1_goodput.csv": 0, "bulk2_goodput.csv": 0, "iot3_goodput.csv": 0, "iot4_goodput.csv": 0, "iot5_goodput.csv": 0}

    # Iterate over devices and runs to calculate average goodput
    for device, value in devices.items():
        for run in range(0, 10):
            devices[device] += calculate_average(generate_file_path(szenario, run), device, "goodput")
        devices[device] /= 10

    # Calculate the denominator of Jain's Fairness Index
    sum_goodput_squared = sum(val ** 2 for val in devices.values())

    # Calculate the numerator of Jain's Fairness Index
    sum_goodput = sum(devices.values())
    numerator = sum_goodput ** 2

    # Calculate Jain's Fairness Index
    jains_fairness_index = numerator / (len(devices) * sum_goodput_squared)

    return jains_fairness_index


# Function to generate the file path
def generate_file_path(szenario, run):
    return f'{szenario}/Run_{run}/'



# Function to create a confidence interval (CI) plot
def create_ci_plot(x_labels, y, lower_values, upper_values, y_label, x_label, title):
    x_axis = np.arange(len(x_labels))

    plt.scatter(x_axis, y, color='green', label='Average')
    plt.errorbar(x_axis, y, yerr=[lower_values, upper_values], fmt='.', capsize=5, capthick=3, markersize=10)

    plt.xticks(x_axis, x_labels)
    plt.ylim(bottom=0)

    plt.title(title)
    plt.ylabel(y_label)
    plt.xlabel(x_label)

    plt.show()


# Main script execution
if __name__ == '__main__':
    x_labels = ['CUBIC', 'BIC', 'Cubic/BBR']

    iot_avg_goodput, iot_lower_goodput, iot_upper_goodput = [], [], []
    iot_avg_transmission, iot_lower_transmission, iot_upper_transmission = [], [], []
    bulk_avg_goodput = []

    outputAlicja1 = "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/outputAlicja1/"  
    outputAlicja2 = "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/outputAlicja2/"  
    outputAlicja3 = "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/outputAlicja3/"  
    
    scenarios = [outputAlicja1, outputAlicja2, outputAlicja3]

    for szenario in scenarios:
        print(f"Scenario: {szenario}")
        
        # Initialize the lists here
        iot_avg_goodput_scenario, bulk_avg_goodput_scenario, iot_avg_transmission_scenario = [], [], []
        
        for run in range(0, 10):
            iot_avg_goodput_values = 0
            bulk_avg_goodput_values = 0
            for i in range(3, 5):
                iot_avg_goodput_values += calculate_average(generate_file_path(szenario, run), f"iot{i}_goodput.csv", "goodput")
            for i in range(1, 2):
                bulk_avg_goodput_values += calculate_average(generate_file_path(szenario, run), f"bulk{i}_goodput.csv", "goodput")

            iot_avg_goodput_scenario.append(iot_avg_goodput_values / 3)
            bulk_avg_goodput_scenario.append(bulk_avg_goodput_values / 2)
            iot_avg_transmission_scenario.append(calculate_average(generate_file_path(szenario, run), "transmission_times.csv", "transmission"))

        # Move the print statement outside the inner loop
        print(f"Path: {generate_file_path(szenario, run)}")

        avg_iot_goodput = np.mean(iot_avg_goodput_scenario)
        avg_iot_transmission = np.mean(iot_avg_transmission_scenario)
        avg_bulk_goodput = np.mean(bulk_avg_goodput_scenario)

        avg_iot_goodput = np.mean(iot_avg_goodput_scenario)
        avg_iot_transmission = np.mean(iot_avg_transmission_scenario)
        avg_bulk_goodput = np.mean(bulk_avg_goodput_scenario)

        sem_avg = st.sem(iot_avg_goodput_scenario)
        sem_avg_transmission = st.sem(iot_avg_transmission_scenario)

        lower_iot_goodput, upper_iot_goodput = st.norm.interval(confidence=0.95, loc=avg_iot_goodput, scale=sem_avg)
        lower_iot_transmission, upper_iot_transmission = st.norm.interval(confidence=0.95, loc=avg_iot_transmission, scale=sem_avg_transmission)

        iot_avg_goodput.append(avg_iot_goodput)
        iot_lower_goodput.append(avg_iot_goodput - lower_iot_goodput)
        iot_upper_goodput.append(upper_iot_goodput - avg_iot_goodput)
        iot_avg_transmission.append(avg_iot_transmission)
        iot_lower_transmission.append(avg_iot_transmission - lower_iot_transmission)
        iot_upper_transmission.append(upper_iot_transmission - avg_iot_transmission)
        bulk_avg_goodput.append(avg_bulk_goodput)

    # Correct indentation for the lines after the outer loop
    create_ci_plot(x_labels, iot_avg_goodput, iot_lower_goodput, iot_upper_goodput, "Goodput in Mbit/s", "TCP Versions", "Goodput of IoT Clients")
    create_ci_plot(x_labels, iot_avg_transmission, iot_lower_transmission, iot_upper_transmission, "Transmission Time in Seconds", "TCP Versions", "Transmission Time of IoT Clients")

    print(iot_avg_goodput)
    print(bulk_avg_goodput)

    values = {
        'IoT': iot_avg_goodput,
        'Bulk': bulk_avg_goodput,
    }

    x = np.arange(len(x_labels))
    width = 0.1
    multiplier = 0

    fig, ax = plt.subplots()

    for attribute, measurement in values.items():
        offset = width * multiplier
        rects = ax.bar(x + offset, measurement, width, label=attribute)
        multiplier += 1

    ax.set_ylabel('Time in Nanoseconds')
    ax.set_xlabel('TCP Versions')
    ax.set_title('Goodput of IoT and Bulk Clients')
    ax.set_xticks(x + width * (multiplier - 1) / 2)
    ax.set_xticklabels(x_labels)
    ax.legend(loc='upper left', ncols=2)
    ax.set_ylim(0, 20)

    plt.show()

    print("Jain's fairness index:")
    print(f"CUBIC: {calculate_fairness(outputAlicja1)}")
    print(f"Bic: {calculate_fairness(outputAlicja2)}")
    print(f"CUBIC/BBR: {calculate_fairness(outputAlicja3)}")