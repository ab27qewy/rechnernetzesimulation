# Install script for directory: /Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "default")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/Library/Developer/CommandLineTools/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/build/lib/libns3.40-network-default.dylib")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libns3.40-network-default.dylib" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libns3.40-network-default.dylib")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/build/lib"
      -add_rpath "/usr/local/lib:$ORIGIN/:$ORIGIN/../lib"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libns3.40-network-default.dylib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Library/Developer/CommandLineTools/usr/bin/strip" -x "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libns3.40-network-default.dylib")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/ns3" TYPE FILE FILES
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/helper/application-container.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/helper/delay-jitter-estimation.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/helper/net-device-container.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/helper/node-container.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/helper/packet-socket-helper.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/helper/simple-net-device-helper.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/helper/trace-helper.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/model/address.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/model/application.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/model/buffer.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/model/byte-tag-list.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/model/channel-list.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/model/channel.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/model/chunk.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/model/header.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/model/net-device.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/model/nix-vector.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/model/node-list.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/model/node.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/model/packet-metadata.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/model/packet-tag-list.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/model/packet.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/model/socket-factory.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/model/socket.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/model/tag-buffer.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/model/tag.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/model/trailer.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/test/header-serialization-test.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/address-utils.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/bit-deserializer.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/bit-serializer.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/crc32.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/data-rate.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/drop-tail-queue.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/dynamic-queue-limits.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/error-channel.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/error-model.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/ethernet-header.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/ethernet-trailer.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/flow-id-tag.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/generic-phy.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/inet-socket-address.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/inet6-socket-address.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/ipv4-address.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/ipv6-address.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/llc-snap-header.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/lollipop-counter.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/mac16-address.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/mac48-address.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/mac64-address.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/mac8-address.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/net-device-queue-interface.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/output-stream-wrapper.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/packet-burst.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/packet-data-calculators.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/packet-probe.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/packet-socket-address.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/packet-socket-client.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/packet-socket-factory.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/packet-socket-server.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/packet-socket.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/packetbb.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/pcap-file-wrapper.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/pcap-file.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/pcap-test.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/queue-fwd.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/queue-item.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/queue-limits.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/queue-size.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/queue.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/radiotap-header.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/sequence-number.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/simple-channel.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/simple-net-device.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/sll-header.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/src/network/utils/timestamp-tag.h"
    "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/build/include/ns3/network-module.h"
    )
endif()

