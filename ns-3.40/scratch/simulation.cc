#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/wifi-module.h"
#include "ns3/trace-helper.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/ipv4-address.h"
#include "ns3/csma-module.h"
#include "ns3/mobility-module.h"
#include "ns3/traffic-control-module.h"
#include "ns3/packet-sink.h"
#include "ns3/packet-sink-helper.h"
#include "ns3/bulk-send-helper.h"
#include "ns3/address.h"
#include "ns3/application.h"
#include "ns3/ptr.h"
#include "ns3/tcp-socket-base.h"
#include "ns3/tcp-socket-state.h"
#include "ns3/traced-callback.h"
#include "ns3/log.h"

#include "ns3/tcp-cubic.h"
#include "ns3/tcp-bic.h"
#include "ns3/tcp-bbr.h"

//
#include "ns3/boolean.h"
#include "ns3/callback.h"
#include "ns3/log.h"
#include "ns3/node.h"
#include "ns3/nstime.h"
#include "ns3/packet.h"
#include "ns3/simulator.h"
#include "ns3/socket-factory.h"
#include "ns3/socket.h"
#include "ns3/tcp-socket-base.h"
#include "ns3/tcp-socket-factory.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/traced-callback.h"
#include "ns3/uinteger.h"
#include "ns3/log.h"

/**
 * \ingroup iot
 * \defgroup iot IoTApplication
 */

//NS_LOG_COMPONENT_DEFINE("NetzwerkSimulation");
namespace ns3
{
    
class Socket;

class TcpSocketBaseWrapper : public TcpSocketBase
{
  public:
    TcpSocketBaseWrapper(const TcpSocketBase& sock);

    Ptr<TcpSocketState> GetTcpSocketState();
};

/**
 * \ingroup iot
 *
 * \brief Periodically send data.
 */
class IoTApplication : public Application

{
  public:
    /**
     * \brief Get the type ID.
     * \return the object TypeId
     */
    static TypeId GetTypeId();

    IoTApplication();
    ~IoTApplication() override;

  protected:
    void DoDispose() override;

  private:
    Ptr<Socket> m_socket;                //!< Associated socket
    Address m_peer;                      //!< Peer address
    Address m_local;                     //!< Local address to bind to
    bool m_connected;                    //!< True if connected
    uint32_t m_sendSize;                 //!< Size of data to send each time
    uint64_t m_transmissionBytes;        //!< Limit total number of bytes sent
    uint64_t m_suspendTime;              //!< Limit total number of bytes sent
    uint64_t m_currentTransmissionBytes; //!< Bytes send during current tranmission
    uint64_t m_totBytes;                 //!< Total bytes sent so far
    TypeId m_protocol;                   //!< The type of protocol to use.
    uint32_t m_seq{0};                   //!< Sequence
    Ptr<Packet> m_unsentPacket;          //!< Variable to cache unsent packet
    uint32_t m_maxAmpduSize;

    Time m_tranmissionStart;
    bool m_suspended;
    bool m_skipSlowStart;
    uint32_t m_lastCWnd;

    /// Traced Callback: finished one transmission
    TracedCallback<const Address&, const Time&> m_transmissionFinishTrace;
    TracedCallback<uint32_t, Ipv4Address> m_addSocketTraces;

    // inherited from Application base class.
    void StartApplication() override; // Called at time specified by Start
    void StopApplication() override;  // Called at time specified by Stop

    /**
     * \brief Send data until the L4 transmission buffer is full.
     * \param from From address
     * \param to To address
     */
    void SendData(const Address& from, const Address& to);

    /**
     * \brief Begin a new tranmission of application bytes.
     *
     * Create new socket and start tranmission of data.
     */
    void BeginTransmission();
    /**
     * \brief Connection Succeeded (called by Socket through a callback)
     * \param socket the connected socket
     */
    void ConnectionSucceeded(Ptr<Socket> socket);
    /**
     * \brief Connection Failed (called by Socket through a callback)
     * \param socket the connected socket
     */
    void ConnectionFailed(Ptr<Socket> socket);
    /**
     * \brief Send more data as soon as some has been transmitted.
     *
     * Used in socket's SetSendCallback - params are forced by it.
     *
     * \param socket socket to use
     * \param unused actually unused
     */
    void ContinueSending(Ptr<Socket> socket, uint32_t);
};


TcpSocketBaseWrapper::TcpSocketBaseWrapper(const TcpSocketBase& sock)
    : TcpSocketBase(sock)
{
}

Ptr<TcpSocketState>
TcpSocketBaseWrapper::GetTcpSocketState()
{
    return m_tcb;
}



NS_LOG_COMPONENT_DEFINE("IoTApplication");

NS_OBJECT_ENSURE_REGISTERED(IoTApplication);

TypeId
IoTApplication::GetTypeId(void)
{
    static TypeId tid =
        TypeId("ns3::IoTApplication")
            .SetParent<Application>()
            .SetGroupName("Applications")
            .AddConstructor<IoTApplication>()
            .AddAttribute("SendSize",
                          "The amount of data to send each time.",
                          UintegerValue(1000),
                          MakeUintegerAccessor(&IoTApplication::m_sendSize),
                          MakeUintegerChecker<uint32_t>(1))
            .AddAttribute("Remote",
                          "The address of the destination",
                          AddressValue(),
                          MakeAddressAccessor(&IoTApplication::m_peer),
                          MakeAddressChecker())
            .AddAttribute("TranmissionBytes",
                          "The number of bytes per transmission.",
                          UintegerValue(1000000),
                          MakeUintegerAccessor(&IoTApplication::m_transmissionBytes),
                          MakeUintegerChecker<uint64_t>(1))
            .AddAttribute("SuspensionTime",
                          "Set the time the application waits before a subsequent "
                          "tranmission begins.",
                          UintegerValue(2000),
                          MakeUintegerAccessor(&IoTApplication::m_suspendTime),
                          MakeUintegerChecker<uint64_t>(1500))
            .AddAttribute("Local",
                          "The Address on which to bind the socket. If not set, it "
                          "is generated automatically.",
                          AddressValue(),
                          MakeAddressAccessor(&IoTApplication::m_local),
                          MakeAddressChecker())
            .AddAttribute("Protocol",
                          "The type of protocol to use.",
                          TypeIdValue(TcpSocketFactory::GetTypeId()),
                          MakeTypeIdAccessor(&IoTApplication::m_protocol),
                          MakeTypeIdChecker())
            .AddTraceSource("TransmissionFinish",
                            "A transmission of data has finished.",
                            MakeTraceSourceAccessor(&IoTApplication::m_transmissionFinishTrace),
                            "ns3::IoTApplication::TransmissionFinishCallback")
            .AddAttribute("MaxAmpduSize",
                          "If 0 IoT application will just send whenever possible, otherwise data "
                          "is bufferd untile the MaxAmpduSize can be send at once",
                          UintegerValue(65535),
                          MakeUintegerAccessor(&IoTApplication::m_maxAmpduSize),
                          MakeUintegerChecker<uint32_t>(0, 15523200))
            .AddAttribute("SkipSlowStart",
                          "Whether to skip slow start and reuse cWnd from preceeding "
                          "transmission",
                          BooleanValue(true),
                          MakeBooleanAccessor(&IoTApplication::m_skipSlowStart),
                          MakeBooleanChecker())
            .AddTraceSource("SocketTraces",
                            "When to add socket traces",
                            MakeTraceSourceAccessor(&IoTApplication::m_addSocketTraces),
                            "ns3::IoTApp::AddTraceCallback");

    return tid;
}

IoTApplication::IoTApplication()
    : m_socket(nullptr),
      m_connected(false),
      m_totBytes(0),
      m_unsentPacket(nullptr),
      m_suspended(false),
      m_lastCWnd(0)
{
    NS_LOG_FUNCTION(this);
}

IoTApplication::~IoTApplication()
{
    NS_LOG_FUNCTION(this);
}

void
IoTApplication::DoDispose()
{
    NS_LOG_FUNCTION(this);

    m_socket = nullptr;
    m_unsentPacket = nullptr;
    // chain up
    Application::DoDispose();
}

void
IoTApplication::StartApplication() // Called at time specified by Start
{
    NS_LOG_FUNCTION(this);

    BeginTransmission();
}

void
IoTApplication::StopApplication() // Called at time specified by Stop
{
    NS_LOG_FUNCTION(this);

    if (m_socket)
    {
        m_socket->Close();
        m_connected = false;
    }
}

void
IoTApplication::BeginTransmission()
{
    NS_LOG_FUNCTION(this);
    m_tranmissionStart = Simulator::Now();
    m_currentTransmissionBytes = 0;
    m_socket = Socket::CreateSocket(m_node, m_protocol);

    if (m_skipSlowStart && m_lastCWnd)
    {
        Ptr<TcpSocketBase> base = DynamicCast<TcpSocketBase>(m_socket);
        Ptr<TcpSocketBaseWrapper> extendedBase = new TcpSocketBaseWrapper(*base);
        Ptr<TcpSocketState> state = extendedBase->GetTcpSocketState();
        state->m_initialCWnd = m_lastCWnd / state->m_segmentSize;
        state->m_initialSsThresh = m_lastCWnd;
        m_socket = extendedBase;
    }

    m_socket->SetConnectCallback(MakeCallback(&IoTApplication::ConnectionSucceeded, this),
                                 MakeCallback(&IoTApplication::ConnectionFailed, this));

    m_socket->SetSendCallback(MakeCallback(&IoTApplication::ContinueSending, this));
    m_socket->Bind();
    m_socket->Connect(m_peer);
    m_socket->ShutdownRecv();
}

void
IoTApplication::ContinueSending(Ptr<Socket> socket, uint32_t)
{
    NS_LOG_FUNCTION(this);

    if (m_connected && !m_suspended)
    {
        SendData(m_local, m_peer);
    }
}

void
IoTApplication::SendData(const Address& from, const Address& to)
{
    NS_LOG_FUNCTION(this);

    while (m_currentTransmissionBytes < m_transmissionBytes)
    {
        uint64_t toSend = m_sendSize;

        toSend = std::min(toSend, m_transmissionBytes - m_currentTransmissionBytes);

        NS_LOG_LOGIC("sending packet at " << Simulator::Now());

        Ptr<Packet> packet;
        if (m_unsentPacket)
        {
            packet = m_unsentPacket;
            toSend = packet->GetSize();
        }
        else
        {
            packet = Create<Packet>(toSend);
        }

        int actual = m_socket->Send(packet);
        if ((unsigned)actual == toSend)
        {
            m_totBytes += actual;
            m_currentTransmissionBytes += actual;
            m_unsentPacket = nullptr;
        }
        else if (actual == -1)
        {
            NS_LOG_DEBUG("Unable to send packet; caching for later attempt");
            m_unsentPacket = packet;
            break;
        }
        else if (actual > 0 && (unsigned)actual < toSend)
        {
            // Split the packet into two, save the unsent packet
            Ptr<Packet> sent = packet->CreateFragment(0, actual);
            Ptr<Packet> unsent = packet->CreateFragment(actual, (toSend - (unsigned)actual));
            m_totBytes += actual;
            m_currentTransmissionBytes += actual;
            m_unsentPacket = unsent;
            break;
        }
        else
        {
            NS_FATAL_ERROR("Unexpected return value from m_socket->Send ()");
        }
    }

    if (m_currentTransmissionBytes == m_transmissionBytes && m_connected)
    {
        NS_LOG_DEBUG("Finished Tranmission");
        m_transmissionFinishTrace(m_local, Simulator::Now() - m_tranmissionStart);
        Ptr<TcpSocketBase> base = DynamicCast<TcpSocketBase>(m_socket);
        Ptr<TcpSocketBaseWrapper> extendedBase = new TcpSocketBaseWrapper(*base);
        Ptr<TcpSocketState> state = extendedBase->GetTcpSocketState();
        m_lastCWnd = state->m_cWnd;
        NS_LOG_DEBUG(m_lastCWnd);
        m_socket->Close();
        m_connected = false;
        Simulator::Schedule(MilliSeconds(m_suspendTime), &IoTApplication::BeginTransmission, this);
    }
}

void
IoTApplication::ConnectionSucceeded(Ptr<Socket> socket)
{
    NS_LOG_FUNCTION(this << socket);
    NS_LOG_LOGIC("IoTApplication, Connection succeeded at " << Simulator::Now().As(Time::S));
    m_connected = true;
    m_socket->GetSockName(m_local);
    socket->GetPeerName(m_peer);
    m_addSocketTraces(m_node->GetId(), InetSocketAddress::ConvertFrom(m_local).GetIpv4());
}

void
IoTApplication::ConnectionFailed(Ptr<Socket> socket)
{
    NS_LOG_FUNCTION(this << socket);
    NS_LOG_LOGIC("IoTApplication, Connection failed at " << Simulator::Now().As(Time::S));
}


/**
 * \ingroup bulksend
 * \brief A helper to make it easier to instantiate an ns3::IoTApplication
 * on a set of nodes.
 */
class IoTHelper
{
  public:
    /**
     * Create an IoTHelper to make it easier to work with IoTApplications
     *
     * \param protocol the name of the protocol to use to send traffic
     *        by the applications. This string identifies the socket
     *        factory type used to create sockets for the applications.
     *        A typical value would be ns3::UdpSocketFactory.
     * \param address the address of the remote node to send traffic
     *        to.
     */
    IoTHelper(std::string protocol, Address address);

    /**
     * Helper function used to set the underlying application attributes,
     * _not_ the socket attributes.
     *
     * \param name the name of the application attribute to set
     * \param value the value of the application attribute to set
     */
    void SetAttribute(std::string name, const AttributeValue& value);

    /**
     * Install an ns3::IoTApplication on each node of the input container
     * configured with all the attributes set with SetAttribute.
     *
     * \param c NodeContainer of the set of nodes on which an IoTApplication
     * will be installed.
     * \returns Container of Ptr to the applications installed.
     */
    ApplicationContainer Install(NodeContainer c) const;

    /**
     * Install an ns3::IoTApplication on the node configured with all the
     * attributes set with SetAttribute.
     *
     * \param node The node on which an IoTApplication will be installed.
     * \returns Container of Ptr to the applications installed.
     */
    ApplicationContainer Install(Ptr<Node> node) const;

    /**
     * Install an ns3::IoTApplication on the node configured with all the
     * attributes set with SetAttribute.
     *
     * \param nodeName The node on which an IoTApplication will be installed.
     * \returns Container of Ptr to the applications installed.
     */
    ApplicationContainer Install(std::string nodeName) const;

  private:
    /**
     * Install an ns3::IoTApplication on the node configured with all the
     * attributes set with SetAttribute.
     *
     * \param node The node on which an IoTApplication will be installed.
     * \returns Ptr to the application installed.
     */
    Ptr<Application> InstallPriv(Ptr<Node> node) const;

    ObjectFactory m_factory; //!< Object factory.
};


IoTHelper::IoTHelper(std::string protocol, Address address)
{
    m_factory.SetTypeId("ns3::IoTApplication");
    m_factory.Set("Protocol", StringValue(protocol));
    m_factory.Set("Remote", AddressValue(address));
}

void
IoTHelper::SetAttribute(std::string name, const AttributeValue& value)
{
    m_factory.Set(name, value);
}

ApplicationContainer
IoTHelper::Install(Ptr<Node> node) const
{
    return ApplicationContainer(InstallPriv(node));
}

ApplicationContainer
IoTHelper::Install(std::string nodeName) const
{
    Ptr<Node> node = Names::Find<Node>(nodeName);
    return ApplicationContainer(InstallPriv(node));
}

ApplicationContainer
IoTHelper::Install(NodeContainer c) const
{
    ApplicationContainer apps;
    for (auto i = c.Begin(); i != c.End(); ++i)
    {
        apps.Add(InstallPriv(*i));
    }

    return apps;
}

Ptr<Application>
IoTHelper::InstallPriv(Ptr<Node> node) const
{
    Ptr<Application> app = m_factory.Create<Application>();
    node->AddApplication(app);

    return app;
}
} 


using namespace ns3;

NS_LOG_COMPONENT_DEFINE("NetzwerkSimulation");

AsciiTraceHelper asciiTraceHelper;
Ptr<OutputStreamWrapper> transmissionTimesStream;
Ptr<OutputStreamWrapper> bulkGoodputStream1;
Ptr<OutputStreamWrapper> bulkGoodputStream2;
Ptr<OutputStreamWrapper> iotGoodputStream3;
Ptr<OutputStreamWrapper> iotGoodputStream4;
Ptr<OutputStreamWrapper> iotGoodputStream5;

/*Funktionen*/
static void
TraceTransmissionTimes(const Address& from, const Time& duration)
{
    *transmissionTimesStream->GetStream()
        << InetSocketAddress::ConvertFrom(from).GetIpv4() << "," << duration.GetSeconds() << "\n";
}

static void
TraceGoodput(Ptr<PacketSink> sink,
             int totalPacketsThrough,
             double prevGoodput,
             Ptr<OutputStreamWrapper> output)
{
    double goodput = 0.0;
    totalPacketsThrough = sink->GetTotalRx();
    goodput = (totalPacketsThrough * 8 / (1000000.0)) - prevGoodput;
    *output->GetStream() << Simulator::Now().GetSeconds() << "," << goodput << "\n";
    prevGoodput = (totalPacketsThrough * 8) / (1000000.0);
    Simulator::Schedule(MilliSeconds(25),
                        &TraceGoodput,
                        sink,
                        totalPacketsThrough,
                        prevGoodput,
                        output);
}

int 
main(int argc, char** argv)
{
    CommandLine cmd(__FILE__);
    
    /*uint32_t seed = 341234123;*/
    /*uint32_t seeds[10] = {341287623, 974234123, 642834123, 12467123, 453297651, 734516274, 273540987, 145279836, 835461783, 774235413};*/
    /*uint32_t seeds[10] = {567890123, 876543210, 345678901, 987654321, 543210987, 210987654, 789012345, 123456789, 432109876, 654321098};*/
    uint32_t seeds[10] = {886453599, 981267223, 567846234, 343456789, 654321098, 987654321, 234567897, 987654321, 321456789, 456909012};
    std::string bulk_tcpTypeId;
    std::string iot_tcpTypeId;
    std::string outputDir = "/Users/alicja/Desktop/ns-allinone-3.40 2/ns-3.40/outputAlicja3"; /*for other scenarios change the output path Alicja2 or Alicja3*/
    bool shouldLog = true;
    double duration = 20.0;
    int simulation = 3;  /* 1 - TCP Cubic, 2- TCP BIC, 3-TCP Cubic, BBR */
    int run = 1;   /*1-10 runs*/ 
    int server = 5;

    /*cmd.AddValue("seed", "Seed for random values.", seeds);*/
    cmd.AddValue("bulkTcpType", "Tcp type for bulk flows.", bulk_tcpTypeId);
    cmd.AddValue("iotTcpType", "Tcp type for iot flows.", iot_tcpTypeId);
    cmd.AddValue("output", "Output dir for tracing files.", outputDir);
    cmd.AddValue("duration", "How long the simulation should run.", duration);
    cmd.AddValue("logging", "Enable logging of IoTApplications.", shouldLog);
    cmd.Parse(argc, argv);

    if (shouldLog){
        LogComponentEnable("NetzwerkSimulation", LOG_LEVEL_DEBUG);
    }

    switch(simulation) {
        case 1:
            bulk_tcpTypeId = "TcpCubic";
            iot_tcpTypeId = "TcpCubic";
            break;
        case 2:
            bulk_tcpTypeId = "TcpBic";
            iot_tcpTypeId = "TcpBic";
            break;
        case 3:
            bulk_tcpTypeId = "TcpCubic";
            iot_tcpTypeId = "TcpBbr";
            break;
        default:
            std::cout << "Select szenario [1,2,3]." << std::endl;
            return 0;
    }

    /*std::cout << "Begin simulation with seed: " << seeds << std::endl;*/
    std::cout << "Bulk TCP: " << bulk_tcpTypeId << std::endl;
    std::cout << "IOT TCP: " << iot_tcpTypeId<< std::endl;
    
    Time::SetResolution(Time::NS);
    Config::SetDefault("ns3::TcpSocketState::EnablePacing", BooleanValue(true));

    /*Simulation Run 10 Times*/
    for (int runCount = 0; runCount < 10; runCount++) {
    
    /*Set output directory for this scenario and execute*/
    std::string resultDir = outputDir + "/Run_" + std::to_string(runCount) + "/";

    transmissionTimesStream = asciiTraceHelper.CreateFileStream(resultDir + "transmission_times.csv"); 
    bulkGoodputStream1 = asciiTraceHelper.CreateFileStream(resultDir + "bulk1_goodput.csv");
    bulkGoodputStream2 = asciiTraceHelper.CreateFileStream(resultDir + "bulk2_goodput.csv");
    iotGoodputStream3 = asciiTraceHelper.CreateFileStream(resultDir + "iot3_goodput.csv");
    iotGoodputStream4 = asciiTraceHelper.CreateFileStream(resultDir + "iot4_goodput.csv");
    iotGoodputStream5 = asciiTraceHelper.CreateFileStream(resultDir + "iot5_goodput.csv");

    *transmissionTimesStream->GetStream() << "host,value\n";
    *bulkGoodputStream1->GetStream() << "time,value\n";
    *bulkGoodputStream2->GetStream() << "time,value\n";
    *iotGoodputStream3->GetStream() << "time,value\n";
    *iotGoodputStream4->GetStream() << "time,value\n";
    *iotGoodputStream5->GetStream() << "time,value\n";
    

    /*Get the seed for the run*/
    RngSeedManager::SetSeed(seeds[runCount]);
    std::cout << "Begin simulation with seed: " << seeds[runCount] << std::endl;
    /*P2P*/
    NodeContainer pointToPoint;
    pointToPoint.Create(2);
    // Create point-to-point links between clients and router
    PointToPointHelper p2p;
    /*p2p.SetDeviceAttribute("DataRate", StringValue("100Mbps"));*/
    p2p.SetChannelAttribute("Delay", StringValue("10ms"));
    NetDeviceContainer p2pDevices; 
    p2pDevices = p2p.Install(pointToPoint);
    p2pDevices.Get(0)->SetAttribute("DataRate", StringValue("50Mbps"));
    p2pDevices.Get(1)->SetAttribute("DataRate", StringValue("1Gbps"));

    // NS_LOG_INFO("Point to Point nodes created");
    
    /*CSMA*/
    NodeContainer csmaNodes;
    csmaNodes.Add(pointToPoint.Get(1));
    csmaNodes.Create(server);

    CsmaHelper csma;
    csma.SetChannelAttribute("DataRate", StringValue("2Gbps"));
    NetDeviceContainer csmaDevices;
    csmaDevices = csma.Install(csmaNodes);

    // NS_LOG_INFO("CSMA nodes created");

    /* Wifi */
    NodeContainer clientNodes;
    clientNodes.Create(5);

    NodeContainer wifiAPNode = pointToPoint.Get(0);
    wifiAPNode.Add(clientNodes);

    NodeContainer wifiBulkNodes;
    wifiBulkNodes.Add(clientNodes.Get(0));
    wifiBulkNodes.Add(clientNodes.Get(1));

    NodeContainer wifiIoTNodes;
    wifiIoTNodes.Add(clientNodes.Get(2));
    wifiIoTNodes.Add(clientNodes.Get(3));
    wifiIoTNodes.Add(clientNodes.Get(4));

    // NS_LOG_INFO("Wifi nodes created");

    /*Network config*/
    YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default();
    YansWifiPhyHelper phy;
    phy.SetChannel(wifiChannel.Create());


    // Create WiFi link between router and clients
    WifiHelper wifi;
    wifi.SetRemoteStationManager("ns3::IdealWifiManager");
    wifi.SetStandard(WifiStandard::WIFI_STANDARD_80211ax);
    
    WifiMacHelper wifiMac;
    Ssid ssid = Ssid("ns-3-ssid");
    wifiMac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid), "ActiveProbing", BooleanValue(false));

    NetDeviceContainer clientDevices;
    clientDevices = wifi.Install(phy, wifiMac, clientNodes);

    wifiMac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid));

    NetDeviceContainer apDevice;
    apDevice = wifi.Install(phy, wifiMac, wifiAPNode);

    Ptr<RateErrorModel> emWifi = CreateObject<RateErrorModel>();
    emWifi->SetAttribute("ErrorRate", DoubleValue(0.001));
    emWifi->SetAttribute("ErrorUnit", EnumValue(RateErrorModel::ERROR_UNIT_PACKET));
    emWifi->SetAttribute("IsEnabled", BooleanValue(true));
    for (uint32_t i = 0; i < clientDevices.GetN(); i++)
    {
        Ptr<WifiNetDevice> device = StaticCast<WifiNetDevice>(clientDevices.Get(i));
        device->GetPhy()->SetPostReceptionErrorModel(PointerValue(emWifi));
    }
    
    MobilityHelper mobility;
    mobility.SetPositionAllocator("ns3::UniformDiscPositionAllocator");
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility.Install(wifiAPNode);
    mobility.Install(clientNodes);

    // Install internet stack on all nodes
    InternetStackHelper stack;
    stack.Install(csmaNodes);
    stack.Install(wifiAPNode);
    Config::SetDefault("ns3::TcpL4Protocol::SocketType", StringValue("ns3::" + bulk_tcpTypeId));
    stack.Install(wifiBulkNodes);
    Config::SetDefault("ns3::TcpL4Protocol::SocketType", StringValue("ns3::" + iot_tcpTypeId));
    stack.Install(wifiIoTNodes);

    
    TrafficControlHelper tcHelper;
    tcHelper.SetRootQueueDisc("ns3::FqCoDelQueueDisc");
    tcHelper.Install(apDevice);
    
    // Assign IP addresses
    
    Ipv4AddressHelper address;
    address.SetBase("192.168.1.0", "255.255.255.0");
    Ipv4InterfaceContainer csmaInterfaces = address.Assign(csmaDevices);
    address.SetBase("192.168.2.0", "255.255.255.0");
    Ipv4InterfaceContainer wifiInterfaces = address.Assign(clientDevices);
    Ipv4InterfaceContainer apInterface = address.Assign(apDevice);
    address.SetBase("192.168.3.0", "255.255.255.0");
    Ipv4InterfaceContainer p2pInterfaces = address.Assign(p2pDevices);

    /* Populate routing table */
    Ipv4GlobalRoutingHelper::PopulateRoutingTables();

    // NS_LOG_INFO("Network set up");
    /* Applications*/
    /* Create TCP Receiver */
    PacketSinkHelper sinkHelper("ns3::TcpSocketFactory", InetSocketAddress(Ipv4Address::GetAny(), 9));

    /* Install TCP Receiver (sink) on CSMA Node 1-5 ("Server") */
    ApplicationContainer sinkApps;
    sinkApps.Add(sinkHelper.Install(csmaNodes.Get(0)));
    sinkApps.Add(sinkHelper.Install(csmaNodes.Get(1)));
    sinkApps.Add(sinkHelper.Install(csmaNodes.Get(2)));
    sinkApps.Add(sinkHelper.Install(csmaNodes.Get(3)));
    sinkApps.Add(sinkHelper.Install(csmaNodes.Get(4)));

    /* Create Bulk Application for network nodes 1 and 2 */
    /* Reminder: network node 0 = AP */
    BulkSendHelper bulkApplication("ns3::TcpSocketFactory", InetSocketAddress(csmaInterfaces.GetAddress(0), 9));
    ApplicationContainer bulkApps;
    bulkApps.Add(bulkApplication.Install(clientNodes.Get(0)));
    bulkApplication = BulkSendHelper("ns3::TcpSocketFactory", InetSocketAddress(csmaInterfaces.GetAddress(1), 9));
    bulkApps.Add(bulkApplication.Install(clientNodes.Get(1)));

    
    IoTHelper iotHelper("ns3::TcpSocketFactory", InetSocketAddress(csmaInterfaces.GetAddress(2), 9));
    ApplicationContainer iotApps;
    iotApps.Add(iotHelper.Install(clientNodes.Get(2)));
    iotHelper = IoTHelper("ns3::TcpSocketFactory", InetSocketAddress(csmaInterfaces.GetAddress(3), 9));
    iotApps.Add(iotHelper.Install(clientNodes.Get(3)));
    iotHelper = IoTHelper("ns3::TcpSocketFactory", InetSocketAddress(csmaInterfaces.GetAddress(4), 9));
    iotApps.Add(iotHelper.Install(clientNodes.Get(4)));
    
    Ptr<PacketSink> sinkApp1 = DynamicCast<PacketSink>(sinkApps.Get(0));
    Ptr<PacketSink> sinkApp2 = DynamicCast<PacketSink>(sinkApps.Get(1));
    Ptr<PacketSink> sinkApp3 = DynamicCast<PacketSink>(sinkApps.Get(2));
    Ptr<PacketSink> sinkApp4 = DynamicCast<PacketSink>(sinkApps.Get(3));
    Ptr<PacketSink> sinkApp5 = DynamicCast<PacketSink>(sinkApps.Get(4));

    Config::ConnectWithoutContext("/NodeList/*/ApplicationList/*/$ns3::IoTApplication/TransmissionFinish",MakeCallback(&TraceTransmissionTimes));

    Simulator::Schedule(Seconds(2), &TraceGoodput, sinkApp1, 0, 0, bulkGoodputStream1);
    Simulator::Schedule(Seconds(2), &TraceGoodput, sinkApp2, 0, 0, bulkGoodputStream2);
    Simulator::Schedule(Seconds(2), &TraceGoodput, sinkApp3, 0, 0, iotGoodputStream3);
    Simulator::Schedule(Seconds(2), &TraceGoodput, sinkApp4, 0, 0, iotGoodputStream4);
    Simulator::Schedule(Seconds(2), &TraceGoodput, sinkApp5, 0, 0, iotGoodputStream5);


    /* Start Applications (sink first and clients 2 sec late) */
    sinkApps.Start(Seconds(0.0));
    bulkApps.Start(Seconds(2.0));
    iotApps.Start(Seconds(2.0));
    /* Stop Applications (simulation time +2 seconds to get full sim time) */
    sinkApps.Stop(Seconds(2.0 + duration));
    bulkApps.Stop(Seconds(2.0 + duration));
    iotApps.Stop(Seconds(2.0 + duration));

    /* Start/Stop Simulation */
    Simulator::Stop(Seconds(2.0 + duration));
    Simulator::Run();
    Simulator::Destroy();

    /* Total bytes received per sink, when the simulation finished for current run */
        std::cout << "Total bytes received(Bulk1): " << DynamicCast<PacketSink>(sinkApps.Get(0))->GetTotalRx() << " bytes"<< std::endl;
        std::cout << "Total bytes received(Bulk2): " << DynamicCast<PacketSink>(sinkApps.Get(1))->GetTotalRx() << " bytes"<< std::endl;
        std::cout << "Total bytes received(Iot3): " << DynamicCast<PacketSink>(sinkApps.Get(2))->GetTotalRx() << " bytes"<< std::endl;
        std::cout << "Total bytes received(Iot4): " << DynamicCast<PacketSink>(sinkApps.Get(3))->GetTotalRx() << " bytes"<< std::endl;
        std::cout << "Total bytes received(Iot5): " << DynamicCast<PacketSink>(sinkApps.Get(4))->GetTotalRx() << " bytes"<< std::endl;

        /* Set next run (for output) */
        run++;
    }

    return 0;
}

